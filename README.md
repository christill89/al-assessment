## AL Assessment

To run:
`npm install && npm start`

### Notes:

* This is based on https://github.com/facebook/create-react-app.
* No date picker polyfill has been added for this demo and so this is best demoed in Chrome.
* There are no tests for this application.
* Time to complete. 4.5 hours
