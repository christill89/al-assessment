import React from "react";
import ReactDOM from "react-dom";

import registerServiceWorker from "./registerServiceWorker";

import { Provider } from "react-redux";
import { createStore } from "redux";
import reducers from "./reducers";

import { persistStore, persistReducer } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import storage from "redux-persist/lib/storage";

import { BrowserRouter, Route, Switch } from "react-router-dom";

import AppContainer from "./containers/App";

const config = {
  key: "app",
  storage
};

const reducer = persistReducer(config, reducers);

const store = createStore(
  reducer,
  window.devToolsExtension ? window.devToolsExtension() : f => f
);

let persistor = persistStore(store);

ReactDOM.render(
  <PersistGate persistor={persistor} loading={<p>loading</p>}>
    <Provider store={store}>
      <BrowserRouter>
        <Route
          render={({ location }) => (
            <Switch location={location}>
              <Route path="/" exact component={AppContainer} />
            </Switch>
          )}
        />
      </BrowserRouter>
    </Provider>
  </PersistGate>,
  document.getElementById("root")
);
registerServiceWorker();
