import * as ListActionTypes from "../actionTypes/list";

export const getLists = () => ({
  type: ListActionTypes.GET_LISTS
});

export const toggleAddNewListForm = () => ({
  type: ListActionTypes.TOGGLE_ADD_NEW_LIST_FORM
});

export const updateNewListTitle = input => ({
  type: ListActionTypes.UPDATE_NEW_LIST_TITLE,
  input
});

export const addNewList = list => ({
  type: ListActionTypes.ADD_NEW_LIST,
  list
});

export const toggleAddNewCardForm = listId => ({
  type: ListActionTypes.TOGGLE_ADD_NEW_CARD_FORM,
  listId
});

export const addNewCard = (listId, card) => ({
  type: ListActionTypes.ADD_NEW_CARD,
  listId: listId,
  card: card
});

export const moveCard = (cardId, targetListId) => ({
  type: ListActionTypes.MOVE_CARD,
  cardId: cardId,
  targetListId: targetListId
});
