import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  align-items: center;
  background: #fff;
  box-sizing: border-box;
  display: flex;
  padding: 12px 24px;
  width: 100%;
`;

const Title = styled.div`
  font-weight: bold;
  letter-spacing: 1px;
`;

const User = styled.p`
  align-items: center;
  display: flex;
  font-weight: bold;
  margin-left: auto;

  &:after {
    background: #eee;
    border-radius: 32px;
    border: 1px solid #ddd;
    content: "";
    display: block;
    margin: 0 0 0 12px;
    height: 26px;
    width: 26px;
  }
`;

export default function Navbar(props) {
  return (
    <Wrapper>
      <Title>TRELLOCLONE.IO</Title>
      <User>Mike</User>
    </Wrapper>
  );
}
