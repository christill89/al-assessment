import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import moment from "moment";
import classnames from "classnames";
import star from "../../assets/star.svg";

const Wrapper = styled.div`
  background: #eee;
  box-shadow: 1px 1px 2px 0px rgba(0, 0, 0, 0.29);
  border-radius: 6px;
  cursor: pointer;
  padding: 16px;
  margin-bottom: 12px;
  position: relative;

  &:hover {
    background: #ddd;
  }

  &.is-overdue {
    background: red;
  }

  &.is-due-soon {
    &:before {
      background: transparent url(${star}) no-repeat center center;
      background-size: 25px 25px;
      content: "";
      display: block;
      height: 30px;
      position: absolute;
      right: 10px;
      top: 6px;
      width: 30px;
    }
  }
`;

const Title = styled.p`
  font-weight: bold;
  margin: 0 0 12px;
  padding: 0;
`;

const Description = styled.p`
  background: #fff;
  border-radius: 6px;
  margin: 0;
  padding: 6px;
`;

export default function Card(props) {
  const dueDate = props.data.dueDate;
  const today = moment();
  let isOverdue = false;
  let isDueSoon = false;

  if (dueDate) {
    isOverdue = moment(dueDate).isBefore(today, "day");

    if (!isOverdue) {
      isDueSoon = moment(dueDate).diff(today, "day") <= 3;
    }
  }

  const cardClass = classnames({
    "is-overdue": isOverdue,
    "is-due-soon": isDueSoon
  });

  return (
    <Wrapper className={cardClass}>
      <Title>{props.data.title}</Title>
      <Description>{props.data.description}</Description>
    </Wrapper>
  );
}

Card.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  })
};
