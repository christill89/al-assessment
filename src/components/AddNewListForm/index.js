import React, { Component } from "react";
import { connect } from "react-redux";
import * as ListActionCreators from "../../actions/list";
import AddNewForm from "../AddNewForm";

class AddNewListForm extends Component {
  /**
   * Handle save
   * @return {undefined}
   */
  handleSave = () => {
    if (!this.props.newListTitle) {
      return;
    }

    this.props.addNewList({
      id: 1,
      title: this.props.newListTitle,
      cards: [],
      isAddNewCardFormShowing: false
    });
  };

  cancel = () => {
    this.props.toggleAddNewListForm();
    this.props.updateNewListTitle("");
  };

  /**
   * Render
   * @return {JSX}
   */
  render() {
    const { newListTitle } = this.props;

    return (
      <AddNewForm
        onChange={e => this.props.updateNewListTitle(e.target.value)}
        onSave={this.handleSave}
        onCancel={this.cancel}
        placeholder="Add new list..."
        disabled={!newListTitle}
        value={newListTitle}
      />
    );
  }
}

const mapStateToProps = state => ({
  newListTitle: state.list.newListTitle
});

const mapDispatchToProps = dispatch => ({
  toggleAddNewListForm: () =>
    dispatch(ListActionCreators.toggleAddNewListForm()),
  updateNewListTitle: input =>
    dispatch(ListActionCreators.updateNewListTitle(input)),
  addNewList: list => dispatch(ListActionCreators.addNewList(list))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewListForm);
