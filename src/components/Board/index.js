import React, { Component } from "react";
import { connect } from "react-redux";
import styled from "styled-components";

import * as ListActionCreators from "../../actions/list";
import { DragDropContext } from "react-beautiful-dnd";

import List from "../List";
import AddNewListForm from "../AddNewListForm";

const Wrapper = styled.div`
  padding: 24px 8px 0;
`;

const ListWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  white-space: nowrap;
  overflow-x: auto;
  padding: 0 0 24px;
  height: calc(100vh - 130px);
`;

const NewList = styled.div`
  background: #d80048;
  border-radius: 6px;
  color: rgba(255, 255, 255, 0.7);
  cursor: pointer;
  display: inline-block;
  margin: 0 8px;
  padding: 8px;
  width: 280px;
  flex: 0 0 280px;

  &:hover {
    background: #ad013a;
  }
`;

class Board extends Component {
  /**
   * Constructor
   * @param  {[type]} props [description]
   * @return {[type]}       [description]
   */
  constructor(props) {
    super(props);

    this.onDragEnd = this.onDragEnd.bind(this);
  }

  /**
   * On Drag End
   * @param  {Object} result
   * @return {undefined}
   */
  onDragEnd(result) {
    if (!result.destination) {
      return;
    }

    this.props.moveCard(result.draggableId, result.destination.droppableId);
  }

  /**
   * Render
   * @return {JSX}
   */
  render() {
    const { isAddNewFormShowing } = this.props;

    const lists = this.props.lists.map((list, i) => (
      <List
        key={list.id}
        data={list}
        isAddNewCardFormVisible={list.isAddNewCardFormShowing}
      />
    ));

    return (
      <Wrapper>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <ListWrapper>
            {lists}
            {isAddNewFormShowing ? (
              <AddNewListForm />
            ) : (
              <NewList onClick={this.props.toggleAddNewListForm}>
                Add a list...
              </NewList>
            )}
          </ListWrapper>
        </DragDropContext>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({
  lists: state.list.lists,
  isAddNewFormShowing: state.list.isAddNewFormShowing,
  isAddNewCardFormVisible: state.list.isAddNewCardFormVisible
});

const mapDispatchToProps = dispatch => ({
  getLists: () => dispatch(ListActionCreators.getLists()),
  toggleAddNewListForm: () =>
    dispatch(ListActionCreators.toggleAddNewListForm()),
  moveCard: (cardId, targetListId) =>
    dispatch(ListActionCreators.moveCard(cardId, targetListId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Board);
