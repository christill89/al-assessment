import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Form = styled.form`
  background: #eee;
  border-radius: 6px;
  box-shadow: 1px 2px 5px 0px rgba(0, 0, 0, 0.5);
  display: inline-block;
  flex: 0 0 280px;
  margin: 0 8px 8px;
  padding: 8px;
  max-width: 280px;
  width: calc(100% - 32px);
`;

const Title = styled.input`
  border: 1px solid #d1d1d1;
  border-radius: 6px;
  display: block;
  margin: 0 0 12px;
  padding: 12px;
  outline: none;
  width: calc(100% - 24px);
`;

const Save = styled.button`
  border: 0;
  background: #0035ff;
  border-radius: 6px;
  color: #fff;
  cursor: ${props => (props.disabled ? "not-allowed" : "pointer")}
  opacity: ${props => (props.disabled ? "0.5" : "1")}
  padding: 12px 24px;

  &:hover {
    background: ${props =>
      props.disabled ? "#0035ff" : "rgba(0, 53, 255, 0.8)"};
  }
`;

const Cancel = styled.button`
  border: 0;
  background: #000;
  border-radius: 6px;
  color: #fff;
  cursor: pointer;
  margin: 0 0 0 12px;
  padding: 12px 24px;

  &:hover {
    background: rgba(0, 0, 0, 0.8);
  }
`;

export default function AddNewForm(props) {
  return (
    <Form inset={props.inset} onSubmit={props.onSave}>
      <Title
        type="text"
        onChange={props.onChange}
        value={props.value}
        placeholder={props.placeholder}
      />
      <Save onClick={props.onSave} disabled={props.disabled} type="submit">
        Save
      </Save>
      <Cancel onClick={props.onCancel}>Cancel</Cancel>
    </Form>
  );
}

AddNewForm.propTypes = {
  inset: PropTypes.bool,
  onChange: PropTypes.func,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onSave: PropTypes.func,
  onCancel: PropTypes.func
};
