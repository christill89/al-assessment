import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import styled from "styled-components";

import * as ListActionCreators from "../../actions/list";

import Card from "../Card";
import AddNewCardForm from "../AddNewCardForm";

import { Droppable, Draggable } from "react-beautiful-dnd";

const ListContainer = styled.div`
  background: #fff;
  border-radius: 6px;
  box-shadow: 1px 2px 5px 0px rgba(0, 0, 0, 0.5);
  display: inline-block;
  flex: 0 0 280px;
  margin: 0 8px;
  padding: 8px;
  width: 280px;
`;

const Title = styled.h2`
  font-size: 16px;
  margin: 0;
  padding: 6px 4px;
`;

const CardContainer = styled.div`
  margin: 12px 0 0;
`;

const AddNew = styled.div`
  background: rgba(211, 211, 211, 0.2);
  border: 2px dashed #eee;
  border-radius: 5px;
  cursor: pointer;
  padding: 16px;
  margin-bottom: 12px;

  &:hover {
    background: #eee;
  }
`;

const getItemStyle = (isDragging, draggableStyle) => ({
  ...draggableStyle
});

class List extends Component {
  /**
   * [propTypes description]
   * @type {Object}
   */
  static propTypes = {
    data: PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      cards: PropTypes.array.isRequired
    })
  };

  /**
   * [render description]
   * @return {[type]} [description]
   */
  render() {
    const { data, toggleAddNewCardForm, isAddNewCardFormVisible } = this.props;
    return (
      <ListContainer>
        <Title>{data.title}</Title>
        <CardContainer>
          <Droppable droppableId={`${data.id}`}>
            {(provided, snapshot) => (
              <div ref={provided.innerRef} style={{}}>
                {data.cards.map((card, i) => {
                  return (
                    <Draggable key={card.id} draggableId={card.id} index={i}>
                      {(provided, snapshot) => (
                        <div>
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            style={getItemStyle(
                              snapshot.isDragging,
                              provided.draggableProps.style
                            )}
                          >
                            <Card key={card.id} data={card} />
                          </div>
                          {provided.placeholder}
                        </div>
                      )}
                    </Draggable>
                  );
                })}
                {isAddNewCardFormVisible ? (
                  <AddNewCardForm listId={data.id} />
                ) : (
                  <AddNew onClick={() => toggleAddNewCardForm(data.id)}>
                    Add new...
                  </AddNew>
                )}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </CardContainer>
      </ListContainer>
    );
  }
}

const mapStateToProps = state => ({
  lists: state.list.lists
});

const mapDispatchToProps = dispatch => ({
  toggleAddNewCardForm: listId =>
    dispatch(ListActionCreators.toggleAddNewCardForm(listId))
});

export default connect(mapStateToProps, mapDispatchToProps)(List);
