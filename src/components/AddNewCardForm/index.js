import React, { Component } from "react";
import { connect } from "react-redux";
import * as ListActionCreators from "../../actions/list";
import styled from "styled-components";

const Form = styled.form`
  background: #eee;
  border-radius: 6px;
  box-shadow: 1px 2px 5px 0px rgba(0, 0, 0, 0.5);
  display: inline-block;
  flex: 0 0 280px;
  margin: 0 8px 8px;
  padding: 8px;
  max-width: 280px;
  width: calc(100% - 32px);
`;

const Title = styled.input`
  border: 1px solid #d1d1d1;
  border-radius: 6px;
  display: block;
  margin: 0 0 12px;
  padding: 12px;
  outline: none;
  width: calc(100% - 24px);
`;

const Save = styled.button`
  border: 0;
  background: #0035ff;
  border-radius: 6px;
  color: #fff;
  cursor: ${props => (props.disabled ? "not-allowed" : "pointer")}
  opacity: ${props => (props.disabled ? "0.5" : "1")}
  padding: 12px 24px;

  &:hover {
    background: ${props =>
      props.disabled ? "#0035ff" : "rgba(0, 53, 255, 0.8)"};
  }
`;

const Cancel = styled.button`
  border: 0;
  background: #000;
  border-radius: 6px;
  color: #fff;
  cursor: pointer;
  margin: 0 0 0 12px;
  padding: 12px 24px;

  &:hover {
    background: rgba(0, 0, 0, 0.8);
  }
`;

const Description = styled.textarea`
  border: 1px solid #d1d1d1;
  border-radius: 6px;
  display: block;
  margin: 0 0 12px;
  padding: 12px;
  outline: none;
  width: calc(100% - 24px);
  resize: vertical;
`;

const Due = styled.input`
  border: 1px solid #d1d1d1;
  border-radius: 6px;
  display: block;
  margin: 0 0 12px;
  padding: 12px;
  outline: none;
  width: calc(100% - 24px);
`;

class AddNewListForm extends Component {
  /**
   * [constructor description]
   * @return {[type]} [description]
   */
  constructor(props) {
    super(props);

    this.state = {
      newCardTitle: "",
      newCardDescription: ""
    };
  }

  /**
   * Handle save
   * @return {undefined}
   */
  handleSave = () => {
    if (!this.state.newCardTitle || !this.state.newCardDescription) {
      return;
    }

    this.props.addNewCard(this.props.listId, {
      title: this.state.newCardTitle,
      description: this.state.newCardDescription,
      dueDate: this.state.newCardDueDate
    });
  };

  /**
   * [cancel description]
   * @return {[type]} [description]
   */
  cancel = () => {
    this.props.toggleAddNewCardForm(this.props.listId);
  };

  /**
   * [updateNewCardTitle description]
   * @param  {[type]} input [description]
   * @return {[type]}       [description]
   */
  updateNewCardTitle = input => {
    this.setState({
      newCardTitle: input
    });
  };

  updateNewCardDescription = input => {
    this.setState({
      newCardDescription: input
    });
  };

  updateNewCardDueDate = input => {
    this.setState({
      newCardDueDate: input
    });
  };

  /**
   * Render
   * @return {JSX}
   */
  render() {
    return (
      <Form inset={this.props} onSubmit={this.handleSave}>
        <Title
          type="text"
          onChange={e => this.updateNewCardTitle(e.target.value)}
          value={this.state.newCardTitle}
          placeholder="Add new card..."
        />
        <Description
          onChange={e => this.updateNewCardDescription(e.target.value)}
          value={this.state.newCardDescription}
          placeholder="Card Description..."
        />
        <Due
          type="date"
          onChange={e => this.updateNewCardDueDate(e.target.value)}
          value={this.state.dueDate}
        />
        <Save
          onClick={this.handleSave}
          disabled={!this.state.newCardDescription || !this.state.newCardTitle}
          type="submit"
        >
          Save
        </Save>
        <Cancel onClick={this.cancel} type="button">
          Cancel
        </Cancel>
      </Form>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  updateNewListTitle: input =>
    dispatch(ListActionCreators.updateNewListTitle(input)),
  addNewList: list => dispatch(ListActionCreators.addNewList(list)),
  toggleAddNewCardForm: listId =>
    dispatch(ListActionCreators.toggleAddNewCardForm(listId)),
  addNewCard: (listId, card) =>
    dispatch(ListActionCreators.addNewCard(listId, card))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewListForm);
