import * as ListActionTypes from "../actionTypes/list";
/**
 * Initial state
 * @type {Object}
 */
const initialState = {
  isAddNewFormShowing: false,
  newListTitle: "",
  lists: []
};

/**
 * Albums Reducer
 * @param {Object} state
 * @param {[Object]} action
 */
const List = (state = initialState, action) => {
  switch (action.type) {
    case ListActionTypes.GET_LISTS:
      return {
        ...state
      };

    case ListActionTypes.TOGGLE_ADD_NEW_LIST_FORM:
      return {
        ...state,
        isAddNewFormShowing: state.isAddNewFormShowing ? false : true
      };

    case ListActionTypes.ADD_NEW_LIST:
      const newList = action.list;

      newList.id = state.lists.length + 1;

      return {
        ...state,
        newListTitle: "",
        isAddNewFormShowing: false,
        lists: [...state.lists, newList]
      };

    case ListActionTypes.UPDATE_NEW_LIST_TITLE:
      return {
        ...state,
        newListTitle: action.input
      };

    case ListActionTypes.TOGGLE_ADD_NEW_CARD_FORM:
      const updatedLists = state.lists.map((list, i) => {
        if (list.id === action.listId) {
          list.isAddNewCardFormShowing = list.isAddNewCardFormShowing
            ? false
            : true;
        }

        return list;
      });

      return {
        ...state,
        lists: updatedLists
      };

    case ListActionTypes.ADD_NEW_CARD:
      const updatedListsWithCards = state.lists.map((list, i) => {
        if (list.id === action.listId) {
          const newCard = action.card;
          newCard.id = `${list.id}${list.cards.length + 1}`;
          list.cards = [...list.cards, action.card];
          list.isAddNewCardFormShowing = false;
        }

        return list;
      });

      return {
        ...state,
        lists: updatedListsWithCards
      };

    case ListActionTypes.MOVE_CARD:
      let cardToMove;

      state.lists.map((list, i) => {
        if (!cardToMove) {
          cardToMove = list.cards.find(card => {
            return card.id === action.cardId;
          });
        }

        return list;
      });

      const newLists = state.lists.map((list, i) => {
        // remove that card from all lists
        const index = list.cards.indexOf(cardToMove);
        if (index > -1) {
          list.cards.splice(index, 1);
        }

        // add it to the destination list
        if (list.id.toString() === action.targetListId) {
          list.cards = [...list.cards, cardToMove];
        }

        return list;
      });

      return {
        ...state,
        lists: newLists
      };

    default:
      return state;
  }
};

export default List;
