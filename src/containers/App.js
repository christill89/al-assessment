import React, { Component } from "react";
import Board from "../components/Board";
import { injectGlobal } from "styled-components";
import { normalize } from "polished";
import Navbar from "../components/Navbar";

injectGlobal`
  ${normalize()}

  @import url('https://fonts.googleapis.com/css?family=Montserrat:300');

  body {
    background: #FF0055;
    //font-family: Montserrat;
  }
`;

export default class App extends Component {
  /**
   * Render
   * @return {JSX}
   */
  render() {
    return [<Navbar key="nav" />, <Board key="board" />];
  }
}
