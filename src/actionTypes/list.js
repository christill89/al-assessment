// Get lists
export const GET_LISTS = "list/GET_LISTS";

// Add a new list
export const TOGGLE_ADD_NEW_LIST_FORM = "list/TOGGLE_ADD_NEW_LIST_FORM";
export const UPDATE_NEW_LIST_TITLE = "list/UPDATE_NEW_LIST_TITLE";
export const ADD_NEW_LIST = "list/ADD_NEW_LIST";

// Add a new card
export const TOGGLE_ADD_NEW_CARD_FORM = "list/TOGGLE_ADD_NEW_CARD_FORM";
export const ADD_NEW_CARD = "list/ADD_NEW_CARD";
export const MOVE_CARD = "list/MOVE_CARD";
